PYTHON_PACKAGEVERSION := $(shell ../syndicate-py/print-package-version)

all: python3-syndicate_$(PYTHON_PACKAGEVERSION)_all.deb

python3-syndicate_%_all.deb:
	./build-python-deb

clean:
	rm -f *.deb
	rm -rf build.*
